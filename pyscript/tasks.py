import os
import gc
from io import StringIO, BytesIO
import pickle

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig


from PIL import Image
from importlib import import_module
import importlib.util


@celery.task(serializer='pickle')
def run_script_async_task(project, script, id, image):
    image = Image.open(image)
    print(image)
    class_name = os.path.splitext(os.path.basename(script))[0]
    spec = importlib.util.spec_from_file_location(
        class_name, os.path.join(AppConfig.DATA_FOLDER, project, script))
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    class_attr = getattr(module, class_name)
    class_instance = class_attr(image)
    img = class_instance()
    img_io = BytesIO()
    img.save(img_io, 'JPEG', quality=70)
    redis = AppConfig.SESSION_REDIS
    redis.setex(id, 600, img_io.getvalue())
