import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

from PIL import Image
from importlib import import_module
import importlib.util
import inspect
from werkzeug.utils import secure_filename
from ..tasks import run_script_async_task


class PyscriptController(Controller):

    @route('/scripts', defaults={'project': ''})
    @route('/scripts/<string:project>')
    def pyscript_script(self, project):
        if not project:
            return abort(404)
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        py_files = [f for f in os.listdir(BASE_DIR) if f.endswith('.py')]
        resp = jsonify(scripts=py_files)
        return resp

    def run_script(self, project, script, image):
        id = str(uuid.uuid4())+'.jpg'
        run_script_async_task.delay(project, script, id, image)
        return id

    @route('/run/<string:project>/<string:script>', methods=['POST'])
    def pyscript_run(self, project, script):
        print(project, script)
        if 'filepond' not in request.files:
            return abort(404)
        file = request.files['filepond']
        print(file)
        if file.filename == '':
            return abort(404)
        img = Image.open(file.stream)
        id = self.run_script(project, script, file.stream)
        print('run', file.filename)
        return jsonify({'id': id, 'project': project, 'filename': file.filename,
                        'script': script})

    @route('/image/<string:id>')
    def pyscript_image(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        img_io = BytesIO()
        img = Image.open(BytesIO(cached))
        img.save(img_io, 'JPEG', quality=90)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')

    @route('/thumbnail/<string:id>')
    def pyscript_thumbnail(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        img_io = BytesIO()
        img = Image.open(BytesIO(cached))
        img.thumbnail((128, 128), Image.ANTIALIAS)
        img.save(img_io, 'JPEG', quality=70)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')

    @route('/images/<string:project>')
    def images(self, project):
        redis = AppConfig.SESSION_REDIS
        ids = request.args.get('ids').split(',')
        format = request.args.get('format', "JPEG")
        files = []
        for id in ids:
            cached = redis.get(id)
            if not cached:
                continue
            img_io = BytesIO()
            img = Image.open(BytesIO(cached))
            img.save(img_io, format)
            img_io.seek(0)
            files.append({'id': id, 'data': img_io})

        memory_file = BytesIO()
        with zipfile.ZipFile(memory_file, 'w') as zf:
            for f in files:
                zf.writestr(f['id'], f['data'].getvalue())
        memory_file.seek(0)
        zipfilename = project + "_images.zip"
        return send_file(memory_file, attachment_filename=zipfilename, as_attachment=True)

    @route('/inspect/<string:project>/<string:script>', methods=['GET'])
    def pyscript_inspect(self, project, script):
        class_name = os.path.splitext(os.path.basename(script))[0]
        spec = importlib.util.spec_from_file_location(
            class_name, os.path.join(AppConfig.DATA_FOLDER, project, script))
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        class_attr = getattr(module, class_name)
        docs = class_attr.__doc__
        inspected = inspect.getfullargspec(class_attr.__init__)

        args = {}
        defaults = inspected.defaults
        for arg in inspected.args:
            i = inspected.args.index(arg)
            if defaults and i - len(defaults) >= 0:
                args[arg] = str(defaults[i - len(defaults)])
            else:
                args[arg] = None

        argtypes = {}
        for annotation in inspected.annotations:
            argtypes[annotation] = str(
                inspected.annotations[annotation].__name__)
        return jsonify(project=project, script=script, name=script, docs=docs, args=args, argtypes=argtypes)
