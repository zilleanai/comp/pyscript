import { createRoutine } from 'actions'

export const listPyScripts = createRoutine('pyscript/LIST_PYSCRIPTS')
export const loadPyScriptDetail = createRoutine('tag/LOAD_PYSCRIPT_DETAIL')
