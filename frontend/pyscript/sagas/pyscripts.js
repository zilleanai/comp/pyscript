import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listPyScripts } from 'comps/pyscript/actions'
import PyScriptApi from 'comps/pyscript/api'
import { selectPyScripts } from 'comps/pyscript/reducers/pyscripts'

export const KEY = 'pyscripts'


export const maybeListPyScriptsSaga = function* () {
  const { isLoading, isLoaded } = yield select(selectPyScripts)
  if (!(isLoaded || isLoading)) {
    yield put(listPyScripts.trigger())
  }
}

export const listPyScriptsSaga = createRoutineSaga(
  listPyScripts,
  function* successGenerator() {
    const scripts = yield call(PyScriptApi.listPyScripts)
    yield put(listPyScripts.success({
      scripts,
    }))
  },
)

export default () => [
  takeEvery(listPyScripts.MAYBE_TRIGGER, maybeListPyScriptsSaga),
  takeLatest(listPyScripts.TRIGGER, listPyScriptsSaga),
]
