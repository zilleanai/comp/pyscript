import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadPyScriptDetail } from 'comps/pyscript/actions'
import PyScriptApi from 'comps/pyscript/api'
import { selectPyScriptDetail } from 'comps/pyscript/reducers/pyscriptDetail'


export const KEY = 'PyScriptDetail'

export const maybeLoadPyScriptDetailSaga = function *(script) {
  const { byname, isLoading } = yield select(selectPyScriptDetail)
  const isLoaded = !!byname[script.name]
  if (!(isLoaded || isLoading)) {
    yield put(loadPyScriptDetail.trigger(script))
  }
}

export const loadPyScriptDetailSaga = createRoutineSaga(
  loadPyScriptDetail,
  function *successGenerator({ payload: script }) {

    var s = yield call(PyScriptApi.loadPyScriptDetail, script.script)
    yield put(loadPyScriptDetail.success({ 'script':s }))
  }
)

export default () => [
  takeEvery(loadPyScriptDetail.MAYBE_TRIGGER, maybeLoadPyScriptDetailSaga),
  takeLatest(loadPyScriptDetail.TRIGGER, loadPyScriptDetailSaga),
]
