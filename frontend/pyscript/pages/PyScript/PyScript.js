import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { InfoBox, PageContent } from 'components'
import { PyScriptList, RunScript, ScriptDetail } from 'comps/pyscript/components'

class PyScript extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      script: ''
    }
    this.handleScriptChange = this.handleScriptChange.bind(this);
  }

  handleScriptChange = ({ value: script }) => {
    this.setState({ script: script });
  }

  render() {
    const { isLoaded, error } = this.props
    const { script } = this.state
    if (!isLoaded) {
      return null
    }
    return (
      <PageContent>
        <Helmet>
          <title>PyScript</title>
        </Helmet>
        <h1>PyScript!</h1>
        <label>Script</label>
        <PyScriptList
          onChange={this.handleScriptChange} />
        <ScriptDetail script={script} />
        <label>Images</label>
        <RunScript script={script} />
      </PageContent>)
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    if (isLoaded) {
      return {
        isLoaded
      }
    } else {
      return {
        isLoaded: false
      }
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)


export default compose(
  withConnect,
)(PyScript)
