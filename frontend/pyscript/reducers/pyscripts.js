import { listPyScripts } from 'comps/pyscript/actions'


export const KEY = 'PyScripts'

const initialState = {
  isLoading: false,
  isLoaded: false,
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { scripts } = payload || {}
  switch (type) {
    case listPyScripts.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listPyScripts.SUCCESS:
      return {
        ...state,
        scripts,
        isLoaded: true,
      }

    case listPyScripts.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listPyScripts.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPyScripts = (state) => state[KEY]
export const selectPyScriptsList = (state) => {
  const scripts = selectPyScripts(state)
  return scripts.scripts
}
