import { loadPyScriptDetail } from 'comps/pyscript/actions'


export const KEY = 'pyscriptDetail'

const initialState = {
  isLoading: false,
  names: [],
  byname: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { script } = payload || {}
  const { names, byname } = state

  switch (type) {
    case loadPyScriptDetail.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case loadPyScriptDetail.SUCCESS:
      if (!names.includes(script.name)) {
        names.push(script.name)
      }
      byname[script.name] = script
      return { ...state,
        names,
        byname,
      }

    case loadPyScriptDetail.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPyScriptDetail = (state) => state[KEY]
export const selectPyScriptByName = (state, name) => selectPyScriptDetail(state).byname[name]
