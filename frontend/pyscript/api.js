import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function pyscript(uri) {
  return v1(`/pyscript${uri}`)
}

export default class PyScript {

  static listPyScripts() {
    return get(pyscript(`/scripts/${storage.getProject()}`))
  }

  static loadPyScriptDetail(script) {
    return get(pyscript(`/inspect/${storage.getProject()}/${script}`))
  }

}
