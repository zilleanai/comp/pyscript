import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listPyScripts } from 'comps/pyscript/actions'
import { selectPyScriptsList } from 'comps/pyscript/reducers/pyscripts'
import Select from 'react-select';


import './pyscript-list.scss'

class PyScriptList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.onChange({ value: selectedOption.value })
  }

  componentWillMount() {
    this.props.listPyScripts.trigger()
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  render() {
    const { isLoaded, error, scripts } = this.props
    if (!isLoaded) {
      return null
    }
    const options = this.itemsToOptions(scripts.scripts)
    return (
      <Select
        value={this.state.selectedOption}
        onChange={this.handleChange}
        defaultValue={options[0]}
        name="scripts"
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/pyscript/reducers/pyscripts'))

const withSaga = injectSagas(require('comps/pyscript/sagas/pyscripts'))

const withConnect = connect(
  (state) => {
    const scripts = selectPyScriptsList(state)
    const isLoaded = !!scripts
    return {
      scripts,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listPyScripts }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PyScriptList)
