import React from 'react'
import { compose } from 'redux'
import './run-script.scss'
import { v1 } from 'api'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'
import { ResultImage } from 'comps/pyscript/components'

registerPlugin(FilePondPluginImagePreview);


function pyscript(uri) {
  return v1(`/pyscript${uri}`)
}

class RunScript extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: [],
      results: [],
      resultids: []
    };
  }

  handleInit() {
  }

  render() {
    const { script } = this.props
    const { results, resultids } = this.state
    const server = {
      url: pyscript(`/run/${storage.getProject()}/${script}`),
      timeout: 160000,
      process: {
        onload: (res) => {
          const res_obj = JSON.parse(res)
          const result = {
            id: res_obj['id'],
          }
          const results = this.state.results
          const resultids = this.state.resultids
          results.push(result)
          resultids.push(result.id)
          this.setState({ results: results, resultids: resultids })
          return res;
        }
      }
    }

    return (
      <div className="RunScript">

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={server}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state

            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}
          {this.state.files.map(file => (
            <File key={file} src={file} origin="local" />
          ))}

        </FilePond>
        <label>results:</label>
        <ul className="results">
          {results.map((result, i) => {
            return (
              <li key={i} >
                <ResultImage id={result['id']} />
              </li>
            )
          })}
        </ul>
        <a href={pyscript(`/images/${storage.getProject()}?ids=${resultids.join(',')}`)} download={`${storage.getProject()}_images.zip`}><button>Download!</button></a>
      </div>
    );
  }
}

export default compose(
)(RunScript)
