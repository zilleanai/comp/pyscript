import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import './script-detail.scss'
import { v1 } from 'api'
import { storage } from 'comps/project'

import { loadPyScriptDetail } from 'comps/pyscript/actions'
import { selectPyScriptByName } from 'comps/pyscript/reducers/pyscriptDetail'


class ScriptDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  componentWillMount() {
    const { loadPyScriptDetail, script } = this.props
    loadPyScriptDetail.maybeTrigger({ 'name': script, 'script': script })
  }

  componentWillReceiveProps(nextProps) {
    const { loadPyScriptDetail, script } = nextProps
    if (script != this.props.script) {
      loadPyScriptDetail.maybeTrigger({ 'name': script, 'script': script })
    }
  }

  render() {
    const { isLoaded, script, details } = this.props
    if (!isLoaded) {
      return null
    }
    const { name, args, docs } = details
    let docs_ = null
    if (docs) {
      docs_ = docs.split('\n').map((item, i) => {
        return <p key={i}>{item}</p>;

      });
    }

    return (
      <div className="ScriptDetail">
        {docs_}
        <ul className="args">
          {Object.keys(args).map((key, i) => {
            return (
              <li key={i}>
                <span>
                  {key}={args[key]}
                </span>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

const withReducer = injectReducer(require('comps/pyscript/reducers/pyscriptDetail'))
const withSagas = injectSagas(require('comps/pyscript/sagas/pyscriptDetail'))

const withConnect = connect(
  (state, props) => {
    const { script } = props
    const details = selectPyScriptByName(state, script)
    return {
      script,
      details,
      isLoaded: !!details,
    }
  },
  (dispatch) => bindRoutineCreators({ loadPyScriptDetail }, dispatch),
)

export default compose(
  withReducer,
  withSagas,
  withConnect,
)(ScriptDetail)
