import React from 'react'
import { compose } from 'redux'
import './result-image.scss'
import { v1 } from 'api'

class ResultImage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { selected: false };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({ selected: !this.state.selected });
  }

  render() {
    const { id } = this.props
    const { selected } = this.state
    if (!id) {
      return null
    }
    return (
      selected ? <div className='selected-image'>
        <img onClick={this.onClick} src={v1(`/pyscript/image/${id}`)} />

      </div> :
        <div onClick={this.onClick} className='classified-image'>
          <img src={v1(`/pyscript/thumbnail/${id}`)} /></div>
    )
  }
}

export default compose(
)(ResultImage)
